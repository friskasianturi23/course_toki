/*#include<iostream>
#include<cstdio>
#include<vector>
#include <utility>
#include<algorithm>*/
#include<bits/stdc++.h>

using namespace std;
vector< pair <int,int> > datastack;
vector<int> data;

int addData(int x, int y)
{
    int ctr = 0;
    datastack.push_back(make_pair(x,y));
    for(int i = 0 ; i < datastack.size();i++)
    {
        ctr+=datastack[i].second;
    }
    return ctr;
}
void add_value_to_element(int x){

    for(int i = 0 ; i < datastack.size(); i++)
    {
        datastack[i].first +=x;
    }
}
void extract_value_from_element(int x){
    for(int i = 0 ; i < datastack.size(); i++)
    {
        datastack[i].first-=x;
    }

}
void delete_stack_element(int y)
{
    //data.erase(data.begin()+(data.size()-y), data.end());
    for(int i = datastack.size()-1 ;i >= 0; i--)
    {

        if(datastack[i].second > y)
        {
            datastack[i].second-=y;
            y=0;
        }
        else if(datastack[i].second < y)
        {
            y-=datastack[i].second;
            datastack.erase(datastack.end());
        }
        else
        {
            datastack.erase(datastack.end());
            y=0;
        }
        if(y == 0)break;
    }
}
int main(){

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin>>n;
    while(n--){
        string perintah;
        int x,y;

        cin>>perintah;

        if(perintah == "add")
        {
            cin>>x>>y;
            cout<<addData(x,y)<<endl;
        }
        else if( perintah =="del")
        {
            cin>>y;
            cout<<datastack[datastack.size()-1].first<<endl;
            delete_stack_element(y);

        }
        else if(perintah == "adx")
        {
            cin>>x;
            add_value_to_element(x);
        }
        else
        {
            cin>>x;
            extract_value_from_element(x);
        }

    }
    return 0;
}
