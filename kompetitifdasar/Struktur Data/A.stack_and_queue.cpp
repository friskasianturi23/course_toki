#include<iostream>
#include<cstdio>
#include<queue>
#include<list>
using namespace std;

list<int> data;
int main(){
    int n;
    cin>>n;
    while(n--){
        string perintah;int angka;
        cin>>perintah;
        if(perintah == "push_front")
        {
            cin>>angka;
            data.push_front(angka);
        }
        else if(perintah == "push_back")
        {
            cin>>angka;
            data.push_back(angka);
        }
        else if(perintah == "pop_front")
        {
            data.pop_front();
        }
        else
        {
            data.pop_back();
        }
    }
    for(list<int>::iterator it = data.begin(); it!= data.end(); it++){
        cout<<*it<<endl;
    }

    return 0;
}
