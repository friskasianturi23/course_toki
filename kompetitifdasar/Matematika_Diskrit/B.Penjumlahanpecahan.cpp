#include<bits/stdc++.h>
using namespace std;
int Euclidean(long long A,long long B){
    if(B==0){
        return A;
    }else{
       return  Euclidean(B,A%B);
    }

}
int main(){
    long long A,B,C,D,temp1,temp2,dvs=0;
    cin>>A>>B>>C>>D;
    temp1=(A*D) + (B*C);
    temp2=(B*D);
    dvs=Euclidean(temp1,temp2);
    cout<<temp1/dvs<<" "<<temp2/dvs<<endl;
    return 0;
}
