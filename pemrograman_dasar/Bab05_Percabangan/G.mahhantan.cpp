#include<iostream>

using namespace std;

int absolut(int x)
{
	if(x < 0)
	{
		return -x;
	}
	return x;
}

int main()
{
	long int x1,x2,y1,y2;

	cin>>x1>>y1>>x2>>y2;

	int result;
	result=absolut(x1-x2)+absolut(y1-y2);
	cout<<result<<endl;
}
